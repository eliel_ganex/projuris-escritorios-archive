# Build Image
FROM alpine:3.11.3

LABEL maintainer="Projuris-Ops <blackops@projuris.com.br>"

RUN apk add libc6-compat tzdata

ADD ./swagger /swagger
ADD ./docs /docs

ADD ./dist/api-linux /app

RUN chmod a+x /app

EXPOSE 3000

ENV GIN_MODE=release

CMD ["/app"]