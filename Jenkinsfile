pipeline {
  environment {
    AWS_REGION = "us-east-1"
    AWS_CODEBUILD_PROJECT_NAME = "Docker-Build-ProjurisOne"
    NAMESPACE = "production"
    HELM_RELEASE_NAME = "projurisone-ms-archive-production"
    AUTH_SERVER_URL = "https://api.internal.k8s.projurisone.com.br"
    AUTH_TILLER_TOKEN = "tiller-token-production"
    AUTH_HELM_TLS = "helm-tls-production"
    DEPLOY_BUCKET = "projurisone-deploy"
    DEPLOY_APP_NAME = 'ms-archive'
    DEPLOY_RELEASE = "production"
    DISCORD_URL = "https://discordapp.com/api/webhooks/709548793825067090/5QklLBm-w9X9pU44hlnlr50T3ALgg0EBh4twtRMTiegYNCYmeK26jlqprKaaNKSdQvAJ"
    ECR_REPO = "projurisone-production"
    DOCKERHUB_TOKEN = credentials('projuris@dockerhub')
  }

  options {
    timeout(time: 15, unit: 'MINUTES')
  }

  agent {
    kubernetes {
      label 'pod-golang'
      serviceAccount 'jenkins'
      defaultContainer 'jnlp'
      yaml """
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: jenkins-slave
  annotations:
    iam.amazonaws.com/role: k8s.projurisone.com.br-helm
spec:
  tolerations:
  - key: backend
    operator: Exists
    effect: NoSchedule
  containers:
  - name: jnlp
    image: jenkins/jnlp-slave:alpine
    resources:
      requests:
        memory: 512Mi
        cpu: 100m
      limits:
        memory: 1024Mi
        cpu: 400m
  - name: helm
    image: dtzar/helm-kubectl:3.2.0
    command:
    - cat
    tty: true
    resources:
      requests:
        memory: 128Mi
        cpu: 20m
      limits:
        memory: 256Mi
        cpu: 80m
  - name: golang
    image: golang:1.13
    tty: true
    resources:
      requests:
        memory: 128Mi
        cpu: 100m
      limits:
        memory: 1G
        cpu: 500m
  - name: openapi
    image: openapitools/openapi-generator-cli
    command:
    - cat
    tty: true
    resources:
      requests:
        memory: 128Mi
        cpu: 20m
      limits:
        memory: 256Mi
        cpu: 80m
"""
    }
  }

  stages {

    stage('Send Slack') {
      steps {
        discordSend webhookURL: "${DISCORD_URL}", link: "${env.BUILD_URL}", description: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Started (<${env.BUILD_URL}|Open>)"
      }
    }

    stage('Dependencies') {
      steps {
        container('openapi') {
          sh "rm -rf out/"
          sh "docker-entrypoint.sh generate -i docs/ms-archive.json -g go-gin-server -o out"
          sh "rm -rf out/api out/main.go out/Dockerfile out/.openapi-generator-ignore"
        }
        container('golang') {
          sh "make dependencies"
        }
      }
    }

    stage('Test') {
      steps {
        container('golang') {
          sh "go test -v ./..."
        }
      }
    }

    stage('Build') {
      steps {
        container('golang') {
          sh "make build-docker"
        }
      }
    }

    stage('Upload to S3') {
        steps {
            withAWS(region:"${AWS_REGION}",credentials:'projurisone') {
              s3Upload(bucket:"${DEPLOY_BUCKET}/${DEPLOY_APP_NAME}/${NAMESPACE}", file:'dist/api-linux', path: 'dist');
              s3Upload(bucket:"${DEPLOY_BUCKET}/${DEPLOY_APP_NAME}/${NAMESPACE}", file:'./swagger', path: 'swagger');
              s3Upload(bucket:"${DEPLOY_BUCKET}/${DEPLOY_APP_NAME}/${NAMESPACE}", file:'./docs', path: 'docs');
              s3Upload(bucket:"${DEPLOY_BUCKET}/${DEPLOY_APP_NAME}/${NAMESPACE}", file:'Dockerfile');
            }
        };
    }

    stage('Build Image') {
      steps {
        awsCodeBuild artifactLocationOverride: '', artifactNameOverride: '', artifactNamespaceOverride: '', artifactPackagingOverride: '', artifactPathOverride: '', artifactTypeOverride: '', awsAccessKey: '', awsSecretKey: '', buildSpecFile: '', buildTimeoutOverride: '', computeTypeOverride: 'BUILD_GENERAL1_MEDIUM', credentialsId: '', credentialsType: 'keys', envParameters: '', envVariables: "[ { BUILD_NUMBER, ${BUILD_NUMBER} }, { DEPLOY_BUCKET, ${DEPLOY_BUCKET} }, { DEPLOY_APP_NAME, ${DEPLOY_APP_NAME} }, { DEPLOY_RELEASE, ${DEPLOY_RELEASE} }, { ECR_REPO, ${ECR_REPO} }, { DOCKERHUB_TOKEN, ${DOCKERHUB_TOKEN} } ]", projectName: "${AWS_CODEBUILD_PROJECT_NAME}", proxyHost: '', proxyPort: '', region: "${AWS_REGION}", sourceControlType: 'jenkins', sourceVersion: '', sseAlgorithm: ''
      }
    }

    stage('Deploy Application') {
      steps {
        container('helm') {
          withKubeConfig([credentialsId: "${AUTH_TILLER_TOKEN}",serverUrl: "${AUTH_SERVER_URL}"]) {
            withCredentials([dockerCert(credentialsId: "${AUTH_HELM_TLS}", variable: 'HELM_TLS')]) {

              script {
                sh "helm list --namespace ${NAMESPACE}"

                sh "helm upgrade ${HELM_RELEASE_NAME} ./ms-archive --namespace ${NAMESPACE} -f ./ms-archive/values.yaml \
                --set image.tag=${BUILD_NUMBER} --timeout 10m --atomic --install"
              }
            }
          }
        }
      }
    }

  }

  post {
    always {
        discordSend webhookURL: "${DISCORD_URL}", successful: true, link: "${env.BUILD_URL}", description: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Success (<${env.BUILD_URL}|Open>)"
    }
    failure {
        discordSend webhookURL: "${DISCORD_URL}", successful: false, link: "${env.BUILD_URL}", description: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Failure (<${env.BUILD_URL}|Open>)"
//         container('helm') {
//           withKubeConfig([credentialsId: "${AUTH_TILLER_TOKEN}",serverUrl: "${AUTH_SERVER_URL}"]) {
//             withCredentials([dockerCert(credentialsId: "${AUTH_HELM_TLS}", variable: 'HELM_TLS')]) {
//               sh "helm rollback ${HELM_RELEASE_NAME} 0 --tiller-namespace ${NAMESPACE} --tls --tls-ca-cert ${HELM_TLS}/ca.pem --tls-cert ${HELM_TLS}/cert.pem --tls-key ${HELM_TLS}/key.pem"
//             }
//           }
//         }
        echo 'Deploy finished with error, rollback executed with success'
    }
  }
}
