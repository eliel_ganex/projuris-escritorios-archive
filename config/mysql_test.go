package config

import "testing"

func Test_mysqlURI(t *testing.T) {
	type args struct {
		user      string
		passsword string
		uri       string
		dbname    string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"normal", args{"admin", "admin", "localhost:33221", "one"}, "admin:admin@tcp(localhost:33221)/one?allowNativePasswords=false&checkConnLiveness=false&maxAllowedPacket=0&allowNativePasswords=true&loc=America%2FSao_Paulo&parseTime=true"},
		{"no password", args{"admin", "", "localhost:33221", "one"}, "admin@tcp(localhost:33221)/one?allowNativePasswords=false&checkConnLiveness=false&maxAllowedPacket=0&allowNativePasswords=true&loc=America%2FSao_Paulo&parseTime=true"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mysqlURI(tt.args.user, tt.args.passsword, tt.args.uri, tt.args.dbname); got != tt.want {
				t.Errorf("mysqlURI() = %v, want %v", got, tt.want)
			}
		})
	}
}
