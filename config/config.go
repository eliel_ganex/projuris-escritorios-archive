package config

import (
	"database/sql"
	"github.com/apex/log"
	"os"

	archRepo "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/repository"
	archService "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/service"
	storageRepo "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider/repository"
	"github.com/joho/godotenv"
)

func CreateMysqlRepositories(db *sql.DB) *repository {
	r := &repository{
		ArchiveRepo: archRepo.NewMysqlRepository(db),
		StorageRepo: storageRepo.NewS3Repository(InitS3()),
	}
	return r
}

type repository struct {
	ArchiveRepo *archRepo.MysqlRepository
	StorageRepo *storageRepo.S3Repository
}

// Service init the services
func Service(repo *repository) *service {
	s := &service{
		ArchiveService: archService.NewService(repo.ArchiveRepo, repo.StorageRepo),
	}
	return s
}

type service struct {
	ArchiveService *archService.Service
}

// LoadEnv load the environment from file
func LoadEnv() {
	envFileName := EnvFileName()
	info, _ := os.Stat(envFileName)
	if info != nil {

		log.Infof("Load environment from file %s", envFileName)

		err := godotenv.Load(envFileName)
		if err != nil {
			log.Fatalf("Error loading .env file: %s", err.Error())
		}
	} else {
		log.Infof("Using environment from OS")
	}
}

// LogLevel get environment for log level
func LogLevel() string {
	return os.Getenv("LOG_LEVEL")
}

func EnvFileName() string {
	envFiles := map[string]string{"PRODUCTION": ".env.production", "STAGE": ".env.stage", "DEVELOPMENT": ".env"}
	envFile := envFiles[os.Getenv("APP_ENV")]

	if envFile == "" {
		envFile = envFiles["DEVELOPMENT"]
	}

	return envFile
}
