package config

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
)

const (
	s3Region = "us-east-1"
)

// InitS3 init s3 client from env "AWS_ACCESS_KEY_ID" and "AWS_SECRET_ACCESS_KEY"
func InitS3() *s3.S3 {
	creds := credentials.NewEnvCredentials()

	config := aws.Config{
		Region:      aws.String(s3Region),
		Credentials: creds,
	}

	endpoint := os.Getenv("AWS_ENDPOINT")

	if endpoint != "" {
		config.WithEndpoint(endpoint)
	}

	sess := session.New(&config)

	return s3.New(sess)
}
