package config

import (
	"database/sql"
	"os"
	"time"

	"github.com/apex/log"

	gomysql "github.com/go-sql-driver/mysql"
)

// InitMysql create a new db connection
func InitMysql() *sql.DB {

	uri := mysqlURIFromEnv()

	db, err := sql.Open("mysql", uri)
	if err != nil {
		log.Fatalf("Erro to connect mysql: %s", err.Error())
	}

	db.SetMaxOpenConns(5)
	db.SetMaxIdleConns(2)
	//db.SetConnMaxIdleTime(1 * time.Minute)
	db.SetConnMaxLifetime(time.Hour)

	err = db.Ping()
	if err != nil {
		log.Fatalf("Erro to ping mysql: %s", err.Error())
	}

	return db
}

func mysqlURI(user, password, uri, dbname string) string {

	config := gomysql.Config{
		User:   user,
		Net:    "tcp",
		Addr:   uri,
		DBName: dbname,
		Params: map[string]string{
			"parseTime":            "true",
			"loc":                  "America/Sao_Paulo",
			"allowNativePasswords": "true",
		},
	}

	if len(password) > 0 {
		config.Passwd = password
	}

	return config.FormatDSN()
}

func mysqlURIFromEnv() string {
	return mysqlURI(os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASSWORD"), os.Getenv("MYSQL_URL"), os.Getenv("MYSQL_DBNAME"))
}
