package main

import (
	"bitbucket.org/projuris_enterprise/ms-archive/config"
	archiveApi "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/delivery/api"
	"database/sql"
	"github.com/apex/log/handlers/json"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"github.com/apex/log"

	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
)

func main() {

	log.SetHandler(json.New(os.Stderr))

	config.LoadEnv()

	if config.LogLevel() != "" {
		log.SetLevelFromString(config.LogLevel())
	}

	db := config.InitMysql()
	defer db.Close()

	router := NewRouter(db)

	log.WithError(router.Run(":3000")).Fatal("Start ms")
}

func NewRouter(db *sql.DB) *gin.Engine {

	router := gin.New()
	router.Use(cors.Default())
	router.Use(gzip.Gzip(gzip.DefaultCompression, gzip.WithExcludedPaths([]string{"/api/ping/"})))
	router.Use(gin.LoggerWithConfig(gin.LoggerConfig{SkipPaths: []string{"/api/ping/"}}))
	router.Use(gin.Recovery())

	routerSwagger(router)

	router.GET("/api/", index)
	router.GET("/api/ping/", ping(db))

	r := createRoutes(db)

	for _, route := range r {
		switch route.Method {
		case http.MethodGet:
			router.GET(route.Pattern, route.HandlerFunc)
		case http.MethodPost:
			router.POST(route.Pattern, route.HandlerFunc)
		case http.MethodPut:
			router.PUT(route.Pattern, route.HandlerFunc)
		case http.MethodDelete:
			router.DELETE(route.Pattern, route.HandlerFunc)
		}
	}

	return router
}

func index(c *gin.Context) {
	c.Redirect(301, "/api/swagger")
}

func ping(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {

		err := db.Ping()
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.String(http.StatusOK, "Pong")
	}
}

func createRoutes(db *sql.DB) sw.Routes {
	routes := sw.Routes{}

	repo := config.CreateMysqlRepositories(db)
	service := config.Service(repo)

	routes = append(routes, archiveApi.NewHandler(service.ArchiveService)...)

	return routes
}

func routerSwagger(router *gin.Engine) {
	router.Static("/api/swagger", "./swagger")
	router.Static("/api/docs", "./docs")
}
