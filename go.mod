module bitbucket.org/projuris_enterprise/ms-archive

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/apex/log v1.9.0
	github.com/aws/aws-sdk-go v1.38.1
	github.com/gabriel-vasile/mimetype v1.2.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.5.0
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/steinfletcher/apitest v1.5.3 // indirect
	github.com/steinfletcher/apitest-jsonpath v1.6.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/ziutek/mymysql v1.5.4 // indirect
	gopkg.in/gorp.v1 v1.7.2
)
