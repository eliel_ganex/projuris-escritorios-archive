package archive

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/models"
)

// Repository interface
type Repository interface {
	SaveArchive(archive *models.Archive) (*int64, error)
	FindArchiveByID(archiveID int64) (*models.Archive, error)
	FindArchiveByAccountID(archiveID int64) ([]*models.Archive, error)
	DeleteArchiveByID(archiveID int64) error
	DeleteArchivesByIDs(archives []int64) error
}
