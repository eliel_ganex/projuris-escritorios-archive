package models

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/constants"
	"bitbucket.org/projuris_enterprise/ms-archive/shared/models"
	"database/sql"
	"time"
)

//Archive model
type Archive struct {
	ID                  int64                         `db:"id"`
	AccountID           int64                         `db:"account_id"`
	DateCreated         time.Time                     `db:"created_date"`
	LastUpdated         time.Time                     `db:"last_updated"`
	Description         string                        `db:"description"`
	FileLocation        string                        `db:"file_location"`
	FileSize            int64                         `db:"file_size"`
	FileType            string                        `db:"file_type"`
	Name                string                        `db:"name"`
	S3name              string                        `db:"s3name"`
	UserID              int64                         `db:"user_id"`
	OrigemDocumentoType constants.OrigemDocumentoType `db:"origem_documento_type"`
	ExternalID          sql.NullString                `db:"external_id"`
	Deleted             models.BitBool                `db:"deleted"`
}
