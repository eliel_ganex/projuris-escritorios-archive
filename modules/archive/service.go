package archive

import (
	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
)

// Service interface for archive
type Service interface {
	Upload(archiveDTO sw.ArchiveDto) (id *int64, err error)
	Download(archiveID int64) (archiveDTO *sw.ArchiveDto, err error)
	DeleteByID(archiveID int64) (err error)
	DeleteMultipleByIds(archiveIDs []int64) (err error)
	DeleteByAccountID(accountID int64) (idsFail []int64, err error)
}
