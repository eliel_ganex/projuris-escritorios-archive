package archive

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/models"
	sharedModels "bitbucket.org/projuris_enterprise/ms-archive/shared/models"
	"database/sql"
	"fmt"
	"gopkg.in/gorp.v1"
	"strings"
	"time"
)

// MysqlRepository model
type MysqlRepository struct {
	dbmap *gorp.DbMap
}

// NewMysqlRepository create a new mysqlRepository
func NewMysqlRepository(db *sql.DB) *MysqlRepository {
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"}}
	dbmap.AddTableWithName(models.Archive{}, "archive").SetKeys(true, "id")

	dbmap.TraceOn("[gorp]", &sharedModels.DBLog{})

	return &MysqlRepository{
		dbmap: dbmap,
	}
}

// SaveArchive save an archive to db
func (r *MysqlRepository) SaveArchive(archive *models.Archive) (*int64, error) {
	var err error

	if archive.ID == 0 {
		archive.DateCreated = time.Now()
		archive.LastUpdated = time.Now()

		err = r.dbmap.Insert(archive)
	} else {
		archive.LastUpdated = time.Now()
		_, err = r.dbmap.Update(archive)
	}

	if err != nil {
		return nil, err
	}
	return &archive.ID, nil
}

// DeleteArchiveByID delete archive from db
func (r *MysqlRepository) DeleteArchiveByID(archiveID int64) error {
	_, err := r.dbmap.Exec("delete from archive where id = ?", archiveID)
	if err != nil {
		return err
	}

	return nil
}

// FindArchiveByID find archive by id from db
func (r *MysqlRepository) FindArchiveByID(archiveID int64) (*models.Archive, error) {
	a, err := r.dbmap.Get(models.Archive{}, archiveID)
	if err != nil {
		return nil, err
	}

	if a == nil {
		return nil, nil
	}

	return a.(*models.Archive), err
}

// FindArchiveByAccountID find archives by accountID from db
func (r *MysqlRepository) FindArchiveByAccountID(accountID int64) (archives []*models.Archive, err error) {
	sql := "SELECT id FROM archive WHERE account_id = ?"

	_, err = r.dbmap.Select(&archives, sql, accountID)
	return
}

// DeleteArchivesByIDs delete archives from db
func (r *MysqlRepository) DeleteArchivesByIDs(archiveIDs []int64) error {
	_, err := r.dbmap.Exec("delete from archive where id IN (?)", strings.Trim(strings.Join(strings.Fields(fmt.Sprint(archiveIDs)), ","), "[]"))
	if err != nil {
		return err
	}

	return nil
}
