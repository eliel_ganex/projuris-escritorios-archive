package archive

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/models"
	sharedModels "bitbucket.org/projuris_enterprise/ms-archive/shared/models"
	"database/sql"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestMysqlRepository_SaveArchive(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	repo := NewMysqlRepository(db)

	archive1 := &models.Archive{
		AccountID:           1,
		UserID:              2,
		Description:         "peticao",
		ExternalID:          sql.NullString{String: "111", Valid: true},
		Name:                "peticao.pdf",
		FileLocation:        "1/123_peticao.pdf",
		FileSize:            123,
		FileType:            "pdf",
		OrigemDocumentoType: "Manual",
	}
	archive1.Deleted = false

	mock.ExpectExec("insert into `archive`").
		WithArgs(int64(1), sqlmock.AnyArg(), sqlmock.AnyArg(), "peticao", "1/123_peticao.pdf", 123, "pdf", "peticao.pdf", "", int64(2), "Manual", "111", sharedModels.BitBool(false)).
		WillReturnResult(sqlmock.NewResult(1, 1))

	archive2 := &models.Archive{
		ID:                  22,
		AccountID:           1,
		UserID:              2,
		Description:         "peticao",
		ExternalID:          sql.NullString{String: "111", Valid: true},
		Name:                "peticao.pdf",
		FileLocation:        "1/123_peticao.pdf",
		FileSize:            123,
		FileType:            "pdf",
		OrigemDocumentoType: "Manual",
	}
	archive2.Deleted = false

	mock.ExpectExec("update `archive`").
		WithArgs(int64(1), sqlmock.AnyArg(), sqlmock.AnyArg(), "peticao", "1/123_peticao.pdf", 123, "pdf", "peticao.pdf", "", int64(2), "Manual", "111", sharedModels.BitBool(false), int64(22)).
		WillReturnResult(sqlmock.NewResult(1, 1))

	type args struct {
		archive *models.Archive
	}
	tests := []struct {
		name    string
		args    args
		want    *int64
		wantErr bool
	}{
		{name: "insert", args: args{archive1}, want: &archive1.ID, wantErr: false},
		{name: "update", args: args{archive2}, want: &archive2.ID, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := repo.SaveArchive(tt.args.archive)
			if (err != nil) != tt.wantErr {
				t.Errorf("MysqlRepository.SaveArchive() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("MysqlRepository.SaveArchive() = %v, want %v", got, tt.want)
			}
		})
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func TestMysqlRepository_DeleteArchiveByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	repo := NewMysqlRepository(db)

	mock.ExpectExec("delete from archive").
		WithArgs(int64(111)).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec("delete from archive").
		WithArgs(int64(111)).
		WillReturnError(fmt.Errorf("erro ao excluir arquivo"))

	type args struct {
		archiveID int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "sucess", args: args{archiveID: 111}, wantErr: false},
		{name: "error", args: args{archiveID: 111}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := repo.DeleteArchiveByID(tt.args.archiveID); (err != nil) != tt.wantErr {
				t.Errorf("MysqlRepository.DeleteArchiveByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func TestMysqlRepository_FindArchiveByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	repo := NewMysqlRepository(db)

	columns := []string{"id", "account_id", "created_date", "last_updated", "description", "file_location", "file_size", "file_type", "name", "s3name", "user_id", "origem_documento_type", "external_id", "deleted"}
	mock.ExpectQuery("select .* from `archive`").
		WithArgs(int64(111)).
		WillReturnRows(sqlmock.NewRows(columns).AddRow(int64(111), int64(1), time.Now(), time.Now(), "teste", "loc", 123, "pdf", "arq.pdf", "1/123_arq.pdf", int64(2), "Manual", "123", sharedModels.BitBool(false)))

	mock.ExpectQuery("select .* from `archive`").
		WithArgs(int64(404)).
		WillReturnRows(sqlmock.NewRows(columns))

	mock.ExpectQuery("select .* from `archive`").
		WithArgs(int64(404)).
		WillReturnError(fmt.Errorf("erro ao buscar arquivo"))

	type args struct {
		archiveID int64
	}
	tests := []struct {
		name    string
		args    args
		want    *models.Archive
		wantErr bool
	}{
		{name: "sucess", args: args{archiveID: 111}, want: &models.Archive{ID: 111}, wantErr: false},
		{name: "not found", args: args{archiveID: 404}, want: nil, wantErr: false},
		{name: "error", args: args{archiveID: 404}, want: nil, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := repo.FindArchiveByID(tt.args.archiveID)
			if (err != nil) != tt.wantErr {
				t.Errorf("MysqlRepository.FindArchiveByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				if !reflect.DeepEqual(got.ID, tt.want.ID) {
					t.Errorf("MysqlRepository.FindArchiveByID() = %v, want %v", got.ID, tt.want.ID)
				}
			} else {
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("MysqlRepository.FindArchiveByID() = %v, want %v", got, tt.want)
				}
			}

		})
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		assert.NoError(t, err)
	}
}

func TestMysqlRepository_FindArchiveByAccountID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	repo := NewMysqlRepository(db)

	mock.ExpectExec("SELECT id FROM archive WHERE account_id = ?").
		WithArgs(int64(111)).
		WillReturnError(fmt.Errorf("erro buscar arquivos da conta"))

	type args struct {
		accountID int64
	}
	tests := []struct {
		name         string
		args         args
		wantArchives []*models.Archive
		wantErr      bool
	}{
		{name: "error", args: args{accountID: 111}, wantArchives: nil, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotArchives, err := repo.FindArchiveByAccountID(tt.args.accountID)
			if (err != nil) != tt.wantErr {
				t.Errorf("FindArchiveByAccountID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotArchives, tt.wantArchives) {
				t.Errorf("FindArchiveByAccountID() gotArchives = %v, want %v", gotArchives, tt.wantArchives)
			}
		})
	}
}

func TestMysqlRepository_DeleteArchivesByIDs(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	repo := NewMysqlRepository(db)

	mock.ExpectExec("delete from archive").
		WithArgs("111").
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec("delete from archive").
		WithArgs("111,222").
		WillReturnError(fmt.Errorf("erro ao excluir arquivos"))

	type args struct {
		archiveIDs []int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "sucess", args: args{archiveIDs: []int64{111}}, wantErr: false},
		{name: "error", args: args{archiveIDs: []int64{111, 222}}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := repo.DeleteArchivesByIDs(tt.args.archiveIDs); (err != nil) != tt.wantErr {
				t.Errorf("DeleteArchivesByIDs() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
