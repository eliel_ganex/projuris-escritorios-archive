package api

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive"
	openapi "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Download ...
func Download(archiveService archive.Service) gin.HandlerFunc {

	return func(c *gin.Context) {
		var err error

		defer func() {
			if err != nil {
				c.AbortWithError(500, err)
				return
			}
		}()

		idString := c.Param("id")

		if idString == "" {
			c.AbortWithStatusJSON(400, "Parameter id empty")
			return
		}

		accountID, err := strconv.ParseInt(idString, 10, 64)
		if err != nil {
			return
		}

		archiveDTO, err := archiveService.Download(accountID)
		if err != nil {
			return
		}

		if c.GetHeader("Accept") == "application/json" {
			c.JSON(http.StatusOK, archiveDTO)
		} else {
			err = returnFile(c, archiveDTO)
		}
	}
}

func returnFile(c *gin.Context, archiveDTO *openapi.ArchiveDto) (err error) {
	f, err := ioutil.TempFile("", "download-")
	if err != nil {
		return
	}

	bytes, err := base64.StdEncoding.DecodeString(archiveDTO.Base64)
	if err != nil {
		return
	}

	if _, err = f.Write(bytes); err != nil {
		return
	}
	defer f.Close()

	c.Header("Content-Description", archiveDTO.Description)
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%v", archiveDTO.Name))
	c.Header("Content-Type", http.DetectContentType(bytes))
	c.File(f.Name())
	return
}
