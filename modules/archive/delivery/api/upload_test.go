package api

import (
	archMock "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/mock"
	openapi "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/steinfletcher/apitest"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func Test_Upload(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveDTO := &openapi.ArchiveDto{
		AccountId:   int64(1),
		Description: "Arquivo de documento",
		UserId:      int64(3),
		Base64: "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb2" +
			"4sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzL" +
			"CB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFu" +
			"Y2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGd" +
			"lbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2" +
			"Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=",
	}

	bytes, _ := json.Marshal(archiveDTO)
	stringReader := strings.NewReader(string(bytes))

	archiveID := int64(10)
	service.EXPECT().Upload(gomock.Any()).
		Return(&archiveID, nil).
		Times(1)

	handler := newHandlerUpload(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Request.Body = ioutil.NopCloser(stringReader)
	})

	router.POST(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Post("/v1/archive/upload").
		Expect(t).
		Status(http.StatusOK).
		End()
}

func Test_Upload_500(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	handler := newHandlerUpload(service)

	router := gin.Default()

	router.POST(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Post("/v1/archive/upload").
		Expect(t).
		Status(http.StatusInternalServerError).
		End()
}
