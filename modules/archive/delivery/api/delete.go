package api

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// Delete ...
func DeleteByID(archiveService archive.Service) gin.HandlerFunc {

	return func(c *gin.Context) {
		var err error

		defer func() {
			if err != nil {
				c.AbortWithError(500, err)
				return
			}
		}()

		idString := c.Param("id")

		if idString == "" {
			c.AbortWithStatusJSON(400, "Parameter id empty")
			return
		}

		archiveID, err := strconv.ParseInt(idString, 10, 64)
		if err != nil {
			return
		}

		err = archiveService.DeleteByID(archiveID)
		if err != nil {
			return
		}

		c.JSON(http.StatusOK, nil)
	}
}

// DeleteArchives ...
func DeleteArchives(archiveService archive.Service) gin.HandlerFunc {

	return func(c *gin.Context) {
		var err error

		defer func() {
			if err != nil {
				c.AbortWithError(500, err)
				return
			}
		}()

		var ids []int64
		err = json.NewDecoder(c.Request.Body).Decode(&ids)
		if err != nil {
			return
		}

		err = archiveService.DeleteMultipleByIds(ids)
		if err != nil {
			return
		}

		c.AbortWithStatus(http.StatusOK)
	}
}

// DeleteByAccountID ...
func DeleteByAccountID(archiveService archive.Service) gin.HandlerFunc {

	return func(c *gin.Context) {
		var err error

		defer func() {
			if err != nil {
				c.AbortWithError(500, err)
				return
			}
		}()

		idString := c.Param("id")

		if idString == "" {
			c.AbortWithStatusJSON(400, "Parameter id empty")
			return
		}

		accountID, err := strconv.ParseInt(idString, 10, 64)
		if err != nil {
			return
		}

		idsFail, err := archiveService.DeleteByAccountID(accountID)
		if err != nil {
			return
		}

		c.JSON(http.StatusOK, idsFail)
	}
}
