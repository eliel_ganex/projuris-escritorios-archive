package api

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive"
	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	"net/http"
)

func NewHandler(archiveService archive.Service) []sw.Route {
	return []sw.Route{
		newHandlerUpload(archiveService),
		newHandlerDownload(archiveService),
		newHandlerDeleteByID(archiveService),
		newHandlerDeleteArchives(archiveService),
		newHandlerDeleteByAccountID(archiveService),
	}
}

func newHandlerUpload(archiveService archive.Service) sw.Route {
	return sw.Route{
		Name:        "Upload",
		Method:      http.MethodPost,
		Pattern:     "/v1/archive/upload",
		HandlerFunc: Upload(archiveService),
	}
}

func newHandlerDownload(archiveService archive.Service) sw.Route {
	return sw.Route{
		Name:        "Download",
		Method:      http.MethodGet,
		Pattern:     "/v1/archive/:id/download",
		HandlerFunc: Download(archiveService),
	}
}

func newHandlerDeleteByID(archiveService archive.Service) sw.Route {
	return sw.Route{
		Name:        "DeleteByID",
		Method:      http.MethodDelete,
		Pattern:     "/v1/archive/:id",
		HandlerFunc: DeleteByID(archiveService),
	}
}

func newHandlerDeleteArchives(archiveService archive.Service) sw.Route {
	return sw.Route{
		Name:        "DeleteArchives",
		Method:      http.MethodPost,
		Pattern:     "/v1/delete-archives",
		HandlerFunc: DeleteArchives(archiveService),
	}
}

func newHandlerDeleteByAccountID(archiveService archive.Service) sw.Route {
	return sw.Route{
		Name:        "DeleteByAccountID",
		Method:      http.MethodDelete,
		Pattern:     "/v1/account/:id/archive",
		HandlerFunc: DeleteByAccountID(archiveService),
	}
}
