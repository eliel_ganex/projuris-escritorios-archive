package api

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive"
	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

func Upload(archiveService archive.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		defer func() {
			if err != nil {
				c.AbortWithError(500, err)
				return
			}
		}()

		body, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			return
		}

		var archiveDTO sw.ArchiveDto
		if err = json.Unmarshal(body, &archiveDTO); err != nil {
			return
		}

		id, err := archiveService.Upload(archiveDTO)
		if err != nil {
			return
		}

		c.JSON(http.StatusOK, id)
	}
}
