package api

import (
	archMock "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/mock"
	openapi "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/steinfletcher/apitest"
	jsonpath "github.com/steinfletcher/apitest-jsonpath"
	"net/http"
	"testing"
)

var base64Sample = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb2" +
	"4sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzL" +
	"CB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFu" +
	"Y2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGd" +
	"lbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2" +
	"Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="

var archiveDtoDownloaded = &openapi.ArchiveDto{
	Id:          int64(10),
	AccountId:   int64(1),
	Description: "Arquivo de documento",
	UserId:      int64(3),
	Base64:      base64Sample,
	Name:        "archive.txt",
}

func Test_Download_Json(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveDTO := archiveDtoDownloaded

	archiveID := int64(10)
	service.EXPECT().Download(int64(10)).
		Return(archiveDTO, nil).
		Times(1)

	handler := newHandlerDownload(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", archiveID)
		context.Request.Header.Set("Accept", "application/json")
	})

	router.GET(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Get("/v1/archive/10/download").
		Expect(t).
		Status(http.StatusOK).
		Assert(jsonpath.Equal(`$.id`, float64(10))).
		Assert(jsonpath.Equal(`$.accountId`, float64(1))).
		Assert(jsonpath.Equal(`$.description`, "Arquivo de documento")).
		Assert(jsonpath.Equal(`$.userId`, float64(3))).
		Assert(jsonpath.Equal(`$.base64`, base64Sample)).
		Assert(jsonpath.Equal(`$.name`, "archive.txt")).
		End()
}

func Test_Download_File(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveDTO := archiveDtoDownloaded

	archiveID := int64(10)
	service.EXPECT().Download(int64(10)).
		Return(archiveDTO, nil).
		Times(1)

	handler := newHandlerDownload(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", archiveID)
	})

	router.GET(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Get("/v1/archive/10/download").
		Expect(t).
		Status(http.StatusOK).
		Headers(map[string]string{
			"Content-Description":       "Arquivo de documento",
			"Content-Transfer-Encoding": "binary",
			"Content-Disposition":       "attachment; filename=archive.txt",
			"Content-Type":              "text/plain; charset=utf-8",
		}).
		End()
}

func Test_Download_500(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveID := int64(10)
	service.EXPECT().Download(int64(10)).
		Return(nil, fmt.Errorf("erro ao baixar arquivo")).
		Times(1)

	handler := newHandlerDownload(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", archiveID)
	})

	router.GET(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Get("/v1/archive/10/download").
		Expect(t).
		Status(http.StatusInternalServerError).
		End()
}
