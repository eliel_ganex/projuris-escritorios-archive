package api

import (
	archMock "bitbucket.org/projuris_enterprise/ms-archive/modules/archive/mock"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/steinfletcher/apitest"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func Test_DeleteByID(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveID := int64(10)
	service.EXPECT().DeleteByID(gomock.Eq(int64(10))).
		Times(1)

	handler := newHandlerDeleteByID(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", archiveID)
	})

	router.DELETE(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Delete("/v1/archive/10").
		Expect(t).
		Status(http.StatusOK).
		End()
}

func Test_DeleteByID_500(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	archiveID := int64(10)
	service.EXPECT().DeleteByID(gomock.Eq(int64(10))).
		Return(fmt.Errorf("erro ao excluir arquivo")).
		Times(1)

	handler := newHandlerDeleteByID(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", archiveID)
	})

	router.DELETE(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Delete("/v1/archive/10").
		Expect(t).
		Status(http.StatusInternalServerError).
		End()
}

func Test_DeleteArchives(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	bytes, _ := json.Marshal([]int64{1, 2, 3})
	stringReader := strings.NewReader(string(bytes))

	ids := []int64{1, 2, 3}
	service.EXPECT().DeleteMultipleByIds(gomock.Eq(ids)).
		Times(1)

	handler := newHandlerDeleteArchives(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Request.Body = ioutil.NopCloser(stringReader)
	})

	router.POST(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Post("/v1/delete-archives").
		Expect(t).
		Status(http.StatusOK).
		End()
}

func Test_DeleteArchives_Error(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	bytes, _ := json.Marshal([]int64{1, 2, 3})
	stringReader := strings.NewReader(string(bytes))

	ids := []int64{1, 2, 3}
	service.EXPECT().DeleteMultipleByIds(gomock.Eq(ids)).
		Return(fmt.Errorf("Erro ao excluir arquivos do s3")).
		Times(1)

	handler := newHandlerDeleteArchives(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Request.Body = ioutil.NopCloser(stringReader)
	})

	router.POST(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Post("/v1/delete-archives").
		Expect(t).
		Status(http.StatusInternalServerError).
		//Assert(jsonpath.Equal(`$`, []float64{1,2})).
		End()
}

func Test_DeleteByAccountID(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	accountID := int64(1)
	service.EXPECT().DeleteByAccountID(gomock.Eq(int64(1))).
		Return(nil, nil).
		Times(1)

	handler := newHandlerDeleteByAccountID(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", accountID)
	})

	router.DELETE(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Delete("/v1/account/1/archive").
		Expect(t).
		Status(http.StatusOK).
		End()
}

func Test_DeleteByAccountID_500(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := archMock.NewMockService(controller)

	accountID := int64(1)
	service.EXPECT().DeleteByAccountID(gomock.Eq(int64(1))).
		Return(nil, fmt.Errorf("erro ao excluir pela conta")).
		Times(1)

	handler := newHandlerDeleteByAccountID(service)

	router := gin.Default()

	router.Use(func(context *gin.Context) {
		context.Set("id", accountID)
	})

	router.DELETE(handler.Pattern, handler.HandlerFunc)

	apitest.New().
		Handler(router).
		Delete("/v1/account/1/archive").
		Expect(t).
		Status(http.StatusInternalServerError).
		End()
}
