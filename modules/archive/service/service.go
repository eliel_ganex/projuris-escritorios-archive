package service

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive"
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/constants"
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/models"
	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	storageRepo "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider"
	"database/sql"
	"encoding/base64"
	"fmt"
	"github.com/gabriel-vasile/mimetype"
	"github.com/google/uuid"
	"strings"
)

// Service model
type Service struct {
	repository  archive.Repository
	storageRepo storageRepo.Repository
}

// NewService create a new Service
func NewService(r archive.Repository, storageRepo storageRepo.Repository) *Service {
	return &Service{
		repository:  r,
		storageRepo: storageRepo,
	}
}

// Upload ...
func (s *Service) Upload(archiveDTO sw.ArchiveDto) (id *int64, err error) {
	s3name := fmt.Sprintf("%s_%s", uuid.New().String(), archiveDTO.Name)

	bytes, err := base64.StdEncoding.DecodeString(archiveDTO.Base64)
	if err != nil {
		return
	}

	err = s.storageRepo.Upload(bytes, len(bytes), s3name, archiveDTO.AccountId)
	if err != nil {
		return
	}

	a := models.Archive{
		AccountID:    archiveDTO.AccountId,
		Description:  archiveDTO.Description,
		FileLocation: s.storageRepo.CreateURL(s3name, archiveDTO.AccountId),
		FileSize:     int64(len(bytes)),
		FileType:     strings.ToLower(mimetype.Detect(bytes).Extension()),
		Name:         archiveDTO.Name,
		S3name:       s3name,
		UserID:       archiveDTO.UserId,
	}

	if archiveDTO.ExternalId != nil {
		a.ExternalID = sql.NullString{String: *archiveDTO.ExternalId, Valid: true}
	}

	if archiveDTO.OrigemDocumentoType == "" {
		a.OrigemDocumentoType = constants.Manual
	}

	a.Deleted = false

	id, err = s.repository.SaveArchive(&a)
	if err != nil {
		s.storageRepo.Delete(s3name, archiveDTO.AccountId)
		return
	}

	return
}

// Download ...
func (s *Service) Download(archiveID int64) (archiveDTO *sw.ArchiveDto, err error) {
	archive, err := s.repository.FindArchiveByID(archiveID)
	if err != nil {
		return nil, err
	}

	if archive == nil {
		return nil, fmt.Errorf("Archive não encontrado para o ID %d ", archiveID)
	}

	bytes, err := s.storageRepo.Download(archive.S3name, archive.AccountID)
	if err != nil {
		return
	}

	archiveDTO = &sw.ArchiveDto{}
	archiveDTO.Id = archive.ID
	archiveDTO.Name = archive.Name
	archiveDTO.OrigemDocumentoType = string(archive.OrigemDocumentoType)
	archiveDTO.AccountId = archive.AccountID
	archiveDTO.UserId = archive.UserID
	archiveDTO.FileType = archive.FileType
	archiveDTO.FileSize = archive.FileSize
	archiveDTO.Description = archive.Description
	archiveDTO.Base64 = base64.StdEncoding.EncodeToString(bytes)
	if archive.ExternalID.Valid {
		archiveDTO.ExternalId = &archive.ExternalID.String
	}

	return
}

// DeleteMultipleByIds ...
func (s *Service) DeleteMultipleByIds(archiveIDs []int64) error {

	var objects []storageRepo.ArchiveS3Object

	for _, archiveID := range archiveIDs {

		archive, err := s.findArchiveByID(archiveID)
		if err != nil {
			return err
		}

		objects = append(objects, storageRepo.ArchiveS3Object{Name: archive.S3name, AccountID: archive.AccountID})
	}

	if err := s.storageRepo.DeleteObjects(objects); err != nil {
		return err
	}

	for _, archiveID := range archiveIDs {
		if err := s.repository.DeleteArchiveByID(archiveID); err != nil {
			return err
		}
	}
	return nil
}

// DeleteByAccountID ...
func (s *Service) DeleteByAccountID(accountID int64) (idsFail []int64, err error) {
	archives, err := s.repository.FindArchiveByAccountID(accountID)
	if err != nil {
		return
	}

	for _, archive := range archives {
		if err := s.DeleteByID(archive.ID); err != nil {
			idsFail = append(idsFail, archive.ID)
		}
	}

	return
}

// DeleteByID delete an archive from s3 and mysql
func (s *Service) DeleteByID(archiveID int64) error {
	archive, err := s.findArchiveByID(archiveID)
	if err != nil {
		return err
	}

	err = s.storageRepo.Delete(archive.S3name, archive.AccountID)
	if err != nil {
		return err
	}

	return s.repository.DeleteArchiveByID(archiveID)
}

func (s *Service) findArchiveByID(archiveID int64) (archive *models.Archive, err error) {
	archive, err = s.repository.FindArchiveByID(archiveID)
	if err != nil {
		return
	}

	if archive == nil {
		err = fmt.Errorf("Archive não encontrado para o ID %d ", archiveID)
		return
	}

	return
}
