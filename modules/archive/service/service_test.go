package service

import (
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/constants"
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/mock"
	"bitbucket.org/projuris_enterprise/ms-archive/modules/archive/models"
	sw "bitbucket.org/projuris_enterprise/ms-archive/out/go"
	models2 "bitbucket.org/projuris_enterprise/ms-archive/shared/models"
	storageProvider "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider"
	storageMock "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider/mock"
	"database/sql"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestService_Upload(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	archiveDTO := sw.ArchiveDto{}
	archiveDTO.Name = "archive.txt"
	archiveDTO.AccountId = int64(1)
	archiveDTO.UserId = int64(3)
	archiveDTO.Description = "archive to upload"
	externalID := "ABC"
	archiveDTO.ExternalId = &externalID

	storageRepo.EXPECT().Upload(gomock.Any(), 0, gomock.Any(), int64(1)).Times(1)

	storageRepo.EXPECT().CreateURL(gomock.Any(), gomock.Eq(int64(1))).Times(1)

	archiveID := int64(1)

	repo.EXPECT().SaveArchive(gomock.Any()).DoAndReturn(func(archive *models.Archive) (*int64, error) {

		assert.Empty(t, archive.ID)
		assert.Equal(t, int64(1), archive.AccountID)
		assert.Equal(t, int64(3), archive.UserID)
		assert.Contains(t, archive.S3name, "_archive.txt")
		assert.Equal(t, "archive.txt", archive.Name)
		assert.Equal(t, "archive to upload", archive.Description)
		assert.Equal(t, sql.NullString{String: "ABC", Valid: true}, archive.ExternalID)
		assert.Equal(t, constants.Manual, archive.OrigemDocumentoType)
		assert.Equal(t, models2.BitBool(false), archive.Deleted)

		return &archiveID, nil
	}).Times(1)

	service := NewService(repo, storageRepo)

	type args struct {
		archiveDTO sw.ArchiveDto
	}
	tests := []struct {
		name    string
		args    args
		wantId  *int64
		wantErr bool
	}{
		{name: "success", args: args{archiveDTO: archiveDTO}, wantId: &archiveID, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotId, err := service.Upload(tt.args.archiveDTO)
			if (err != nil) != tt.wantErr {
				t.Errorf("Upload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotId, tt.wantId) {
				t.Errorf("Upload() gotId = %v, want %v", gotId, tt.wantId)
			}
		})
	}
}

func TestService_UploadErrorAndDeleteArchive(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	archiveDTO := sw.ArchiveDto{}
	archiveDTO.AccountId = int64(1)

	storageRepo.EXPECT().Upload(gomock.Any(), 0, gomock.Any(), int64(1)).Times(1)

	storageRepo.EXPECT().CreateURL(gomock.Any(), gomock.Eq(int64(1))).Times(1)

	repo.EXPECT().SaveArchive(gomock.Any()).Return(nil, fmt.Errorf("erro o salvar arquivo")).Times(1)
	storageRepo.EXPECT().Delete(gomock.Any(), int64(1)).Times(1)

	service := NewService(repo, storageRepo)

	type args struct {
		archiveDTO sw.ArchiveDto
	}
	tests := []struct {
		name    string
		args    args
		wantId  *int64
		wantErr bool
	}{
		{name: "error", args: args{archiveDTO: archiveDTO}, wantId: nil, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotId, err := service.Upload(tt.args.archiveDTO)
			if (err != nil) != tt.wantErr {
				t.Errorf("Upload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotId, tt.wantId) {
				t.Errorf("Upload() gotId = %v, want %v", gotId, tt.wantId)
			}
		})
	}
}

func TestService_Download(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	archive := &models.Archive{
		S3name:     "HASH_nomearquivo.txt",
		AccountID:  int64(1),
		ExternalID: sql.NullString{String: "ABC", Valid: true},
	}
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(archive, nil).Times(1)
	storageRepo.EXPECT().Download(gomock.Eq("HASH_nomearquivo.txt"), gomock.Eq(int64(1))).Return(nil, nil).Times(1)

	service := NewService(repo, storageRepo)

	_, err := service.Download(int64(1))

	assert.NoError(t, err)
}

func TestService_DownloadErrors(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(nil, fmt.Errorf("erro ao buscar arquivo pelo accountID")).Times(1)
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(nil, nil).Times(1)

	archive := &models.Archive{
		S3name:    "HASH_nomearquivo.txt",
		AccountID: int64(1),
	}
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(archive, nil).Times(1)
	storageRepo.EXPECT().Download(gomock.Eq("HASH_nomearquivo.txt"), gomock.Eq(int64(1))).Return(nil, fmt.Errorf("erro ao baixar arquivo do s3")).Times(1)

	service := NewService(repo, storageRepo)

	type args struct {
		archiveID int64
	}
	tests := []struct {
		name           string
		args           args
		wantArchiveDTO *sw.ArchiveDto
		wantErr        bool
	}{
		{name: "error FindArchiveByID", args: args{archiveID: int64(1)}, wantErr: true},
		{name: "error file not found", args: args{archiveID: int64(1)}, wantErr: true},
		{name: "error storageRepo.Download", args: args{archiveID: int64(1)}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotArchiveDTO, err := service.Download(tt.args.archiveID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Download() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotArchiveDTO, tt.wantArchiveDTO) {
				t.Errorf("Download() gotArchiveDTO = %v, want %v", gotArchiveDTO, tt.wantArchiveDTO)
			}
		})
	}
}

func TestService_DeleteMultipleByIds(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	archivesID := []int64{
		int64(1),
	}
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(nil, fmt.Errorf("erro ao buscar arquivos pelo accountID")).Times(1)

	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(&models.Archive{ID: 1, AccountID: 2, S3name: "file.pdf"}, nil).Times(1)

	objects := []storageProvider.ArchiveS3Object{{"file.pdf", int64(2)}}
	storageRepo.EXPECT().DeleteObjects(gomock.Eq(objects)).Return(nil).AnyTimes()

	repo.EXPECT().DeleteArchiveByID(gomock.Eq(int64(1))).Return(nil).Times(1)

	service := NewService(repo, storageRepo)

	type args struct {
		archiveIDs []int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "error FindArchiveByID", args: args{archiveIDs: archivesID}, wantErr: true},
		{name: "success", args: args{archiveIDs: archivesID}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := service.DeleteMultipleByIds(tt.args.archiveIDs); (err != nil) != tt.wantErr {
				t.Errorf("DeleteMultipleByIds() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestService_DeleteByAccountID(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	repo.EXPECT().FindArchiveByAccountID(gomock.Eq(int64(1))).Return(nil, nil).Times(1)
	repo.EXPECT().FindArchiveByAccountID(gomock.Eq(int64(1))).Return(nil, fmt.Errorf("erro ao buscar arquivos pelo accountID")).Times(1)

	archives := []*models.Archive{
		{ID: int64(1)},
	}
	repo.EXPECT().FindArchiveByAccountID(gomock.Eq(int64(1))).Return(archives, nil).Times(1)
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(nil, fmt.Errorf("erro ao buscar arquivos pelo accountID")).Times(1)

	service := NewService(repo, storageRepo)

	type args struct {
		accountID int64
	}
	tests := []struct {
		name        string
		args        args
		wantIdsFail []int64
		wantErr     bool
	}{
		{name: "sucess", args: args{accountID: 1}, wantErr: false},
		{name: "error FindArchiveByAccountID", args: args{accountID: 1}, wantErr: true},
		{name: "success ids fail", args: args{accountID: 1}, wantIdsFail: []int64{1}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotIdsFail, err := service.DeleteByAccountID(tt.args.accountID)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteByAccountID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotIdsFail, tt.wantIdsFail) {
				t.Errorf("DeleteByAccountID() gotIdsFail = %v, want %v", gotIdsFail, tt.wantIdsFail)
			}
		})
	}
}

func TestService_DeleteByID(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	service := NewService(repo, storageRepo)

	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(&models.Archive{ID: 1, AccountID: 1, S3name: "123_file.pdf"}, nil).AnyTimes()
	storageRepo.EXPECT().Delete(gomock.Eq("123_file.pdf"), gomock.Eq(int64(1))).Return(nil).AnyTimes()
	repo.EXPECT().DeleteArchiveByID(gomock.Eq(int64(1))).Return(nil).AnyTimes()

	type args struct {
		archiveID int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "sucess", args: args{archiveID: 1}, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := service.DeleteByID(tt.args.archiveID); (err != nil) != tt.wantErr {
				t.Errorf("Service.DeleteByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestService_findArchiveByID(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repo := mock.NewMockRepository(controller)
	storageRepo := storageMock.NewMockRepository(controller)

	service := NewService(repo, storageRepo)

	archive := &models.Archive{ID: 1, AccountID: 1, S3name: "123_file.pdf"}
	repo.EXPECT().FindArchiveByID(gomock.Eq(int64(1))).Return(archive, nil).Times(1)
	repo.EXPECT().
		FindArchiveByID(gomock.Eq(int64(1))).Return(&models.Archive{ID: 1, AccountID: 1, S3name: "not-found"}, nil).
		Return(nil, fmt.Errorf("Erro ao buscar arquivo"))
	repo.EXPECT().
		FindArchiveByID(gomock.Eq(int64(1))).Return(&models.Archive{ID: 1, AccountID: 1, S3name: "404"}, nil).
		Return(nil, nil)

	type args struct {
		archiveID int64
	}
	tests := []struct {
		name        string
		args        args
		wantArchive *models.Archive
		wantErr     bool
	}{
		{name: "sucess", args: args{archiveID: 1}, wantArchive: archive, wantErr: false},
		{name: "error FindArchiveByID", args: args{archiveID: 1}, wantErr: true},
		{name: "error file not found", args: args{archiveID: 1}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotArchive, err := service.findArchiveByID(tt.args.archiveID)
			if (err != nil) != tt.wantErr {
				t.Errorf("findArchiveByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotArchive != tt.wantArchive {
				t.Errorf("findArchiveByID() gotArchive = %v, want %v", gotArchive, tt.wantArchive)
			}
		})
	}
}
