package constants

const (
	//Manual ...
	Manual OrigemDocumentoType = "Manual"

	//Mobile ...
	Mobile OrigemDocumentoType = "Mobile"

	//Push ...
	Push OrigemDocumentoType = "Push"

	//Gerado ...
	Gerado OrigemDocumentoType = "Gerado"
)

// OrigemDocumentoType ...
type OrigemDocumentoType string
