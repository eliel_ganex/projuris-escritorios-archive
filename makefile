gen-openapi:
	rm -rf out/
	docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli:v5.0.0-beta2 generate -i /local/docs/ms-archive.json -g go-gin-server -o /local/out
	rm -rf out/api out/main.go out/Dockerfile out/.openapi-generator-ignore

validate-openapi:
	docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli validate -i /local/docs/ms-archive.json

build-mocks:
	@go get github.com/golang/mock/gomock
	@go install github.com/golang/mock/mockgen
	@~/go/bin/mockgen -destination shared/providers/storage-provider/mock/s3iface.go -package=mock github.com/aws/aws-sdk-go/service/s3/s3iface S3API
	@~/go/bin/mockgen -source=shared/providers/storage-provider/repository.go -destination=shared/providers/storage-provider/mock/repository.go -package=mock
	@~/go/bin/mockgen -source=modules/archive/repository.go -destination=modules/archive/mock/repository.go -package=mock
	@~/go/bin/mockgen -source=modules/archive/service.go -destination=modules/archive/mock/service.go -package=mock

dependencies:
	go mod tidy

clear-dist:
	rm -rf ./dist/

build: dependencies clear-dist build-all

build-docker: clear-dist dependencies build-linux

build-all: build-linux build-mac build-windows

build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./dist/api-linux main.go

build-mac:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o ./dist/api-darwin main.go

build-windows:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o ./dist/api-windows.exe main.go