# MS de arquivos

MS utilizado para comunicação com o serviço S3 da AWS. Responsável pelo upload, download e exclusão de arquivos.

### Env
* MySQL
    * MYSQL_URL
    * MYSQL_DBNAME
    * MYSQL_USER
    * MYSQL_PASSWORD
* AWS
    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * S3_BUCKET

#### Endpoints
* /api - documentação dos endpoints


* POST /v1/archive/upload
* GET /v1/archive/{id}/download
* DELETE /v1/archive/{id}
* POST /v1/delete-archives
* DELETE /v1/account/{id}/archive

### Build via docker

#### Gerar imagem
`docker build -t projurisone/ms-archive -f build.Dockerfile .`

#### Rodar aplicação
`docker-compose up`