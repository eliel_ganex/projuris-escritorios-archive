package storage_provider

// Repository interface
type Repository interface {
	Upload(file []byte, size int, filename string, accountID int64) error
	Download(filename string, accountID int64) (fileBytes []byte, err error)
	Delete(s3name string, accountID int64) error
	DeleteObjects(objects []ArchiveS3Object) error
	CreateURL(filename string, accountID int64) string
}

type ArchiveS3Object struct {
	Name      string
	AccountID int64
}
