package repository

import (
	storageProvider "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider"
	"bytes"
	"fmt"
	"github.com/apex/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/aws/aws-sdk-go/service/s3control"
	"io"
	"net/http"
	"os"
)

// S3Repository model
type S3Repository struct {
	s3API s3iface.S3API
}

// NewS3Repository create a new s3Repository
func NewS3Repository(s3API s3iface.S3API) *S3Repository {
	return &S3Repository{
		s3API: s3API,
	}
}

func (s *S3Repository) Upload(file []byte, size int, filename string, accountID int64) error {

	if S3Bucket() == "" {
		return fmt.Errorf("env not found for %s", "S3Bucket()")
	}

	putObject := &s3.PutObjectInput{
		Bucket:             aws.String(S3Bucket()),
		Key:                aws.String(s3Filename(filename, accountID)),
		Body:               bytes.NewReader(file),
		ContentLength:      aws.Int64(int64(size)),
		ContentType:        aws.String(http.DetectContentType(file)),
		ContentDisposition: aws.String("attachment"),
	}

	getPublicAccessBlockOutput, err := s.s3API.GetPublicAccessBlock(&s3.GetPublicAccessBlockInput{Bucket: aws.String(S3Bucket())})
	if err != nil {
		aerr := err.(awserr.RequestFailure)
		if e, a := s3control.ErrCodeNoSuchPublicAccessBlockConfiguration, aerr.Code(); e == a {
			putObject.ACL = aws.String("public-read")
		} else {
			return err
		}
	}

	if getPublicAccessBlockOutput != nil && getPublicAccessBlockOutput.PublicAccessBlockConfiguration != nil &&
		!*getPublicAccessBlockOutput.PublicAccessBlockConfiguration.BlockPublicAcls {
		putObject.ACL = aws.String("public-read")
	}

	_, err = s.s3API.PutObject(putObject)
	if err != nil {
		return err
	}

	return nil
}

func (s *S3Repository) Download(filename string, accountID int64) (fileBytes []byte, err error) {

	if S3Bucket() == "" {
		return nil, fmt.Errorf("env not found for %s", "S3Bucket()")
	}

	getObject := &s3.GetObjectInput{
		Bucket: aws.String(S3Bucket()),
		Key:    aws.String(s3Filename(filename, accountID)),
	}

	s3Object, err := s.s3API.GetObject(getObject)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchKey:
				return nil, fmt.Errorf("File not found with name %v ", filename)
			}
		}
	}
	if s3Object == nil {
		return nil, fmt.Errorf("File not found with name %v ", filename)
	}

	defer s3Object.Body.Close()

	buf := bytes.NewBuffer(nil)
	if _, err = io.Copy(buf, s3Object.Body); err != nil {
		return
	}
	return buf.Bytes(), nil
}

func (s *S3Repository) Delete(s3name string, accountID int64) error {

	_, err := s.s3API.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(S3Bucket()),
		Key:    aws.String(s3Filename(s3name, accountID)),
	})
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("Delete deleteFileFromS3 could not delete from s3"))
		return err
	}

	return nil
}

func (s *S3Repository) DeleteObjects(objects []storageProvider.ArchiveS3Object) error {

	var s3Objects []*s3.ObjectIdentifier
	for _, object := range objects {
		s3Objects = append(s3Objects, &s3.ObjectIdentifier{
			Key: aws.String(s3Filename(object.Name, object.AccountID)),
		})
	}

	_, err := s.s3API.DeleteObjects(&s3.DeleteObjectsInput{
		Bucket: aws.String(S3Bucket()),
		Delete: &s3.Delete{
			Objects: s3Objects,
			Quiet:   aws.Bool(false),
		},
	})

	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("DeleteObjects deleteFilesFromS3 could not delete from s3"))
		return err
	}

	return nil
}

func (s *S3Repository) CreateURL(filename string, accountID int64) string {
	return fmt.Sprintf("https://%s.s3.amazonaws.com/%s", S3Bucket(), s3Filename(filename, accountID))
}

func s3Filename(filename string, accountID int64) string {
	return fmt.Sprintf("%d/%s", accountID, filename)
}

// S3Bucket return s3 bucket name from env
func S3Bucket() string {
	return os.Getenv("S3_BUCKET")
}
