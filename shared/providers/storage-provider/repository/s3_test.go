package repository

import (
	storageProvider "bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider"
	"bitbucket.org/projuris_enterprise/ms-archive/shared/providers/storage-provider/mock"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3control"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func Test_s3Filename(t *testing.T) {
	type args struct {
		filename  string
		accountID int64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "imagem.png", args: args{filename: "imagem.png", accountID: 1254}, want: "1254/imagem.png"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := s3Filename(tt.args.filename, tt.args.accountID); got != tt.want {
				t.Errorf("s3Filename() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_CreateURL(t *testing.T) {
	os.Setenv("S3_BUCKET", "projurisone-files")

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	repo := NewS3Repository(s3Api)

	type args struct {
		filename  string
		accountID int64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "test 1", args: args{filename: "imagem.png", accountID: 16560}, want: "https://projurisone-files.s3.amazonaws.com/16560/imagem.png"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := repo.CreateURL(tt.args.filename, tt.args.accountID); got != tt.want {
				t.Errorf("createURL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_uploadFileToS3(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		PutObject(gomock.Any()).
		Return(nil, nil).
		Times(1)

	s3Api.EXPECT().
		GetPublicAccessBlock(gomock.Any()).
		Return(&s3.GetPublicAccessBlockOutput{
			PublicAccessBlockConfiguration: &s3.PublicAccessBlockConfiguration{BlockPublicAcls: aws.Bool(true)},
		}, nil).
		Times(1)

	repo := NewS3Repository(s3Api)

	err := repo.Upload(nil, 0, "", 0)

	assert.NoError(t, err)

}

func TestService_uploadFileToS3PutError(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		PutObject(gomock.Any()).
		Return(nil, fmt.Errorf("Erro ao fazer upload do arquivo")).
		Times(1)

	s3Api.EXPECT().
		GetPublicAccessBlock(gomock.Any()).
		Return(nil, nil).
		Times(1)

	repo := NewS3Repository(s3Api)

	err := repo.Upload(nil, 0, "", 0)

	assert.Error(t, err)

}

func TestService_Download(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Object := &s3.GetObjectOutput{}
	stringReader := strings.NewReader("file")
	s3Object.Body = ioutil.NopCloser(stringReader)

	s3Api.EXPECT().
		GetObject(gomock.Any()).
		Return(s3Object, nil).
		Times(1)

	repo := NewS3Repository(s3Api)

	_, err := repo.Download("arquivo.txt", int64(1))

	assert.NoError(t, err)

}

func TestService_DownloadFileNotFound(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		GetObject(gomock.Any()).
		Return(nil, nil).
		Times(1)

	repo := NewS3Repository(s3Api)

	_, err := repo.Download("arquivo.txt", int64(1))

	assert.Error(t, err)

}

func TestService_deleteFileFromS3(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		DeleteObject(gomock.Any()).
		Return(nil, nil)

	repo := NewS3Repository(s3Api)

	err := repo.Delete("image.png", 1465)

	assert.NoError(t, err)
}

func TestService_deleteFileFromS3Error(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		DeleteObject(gomock.Any()).
		Return(nil, fmt.Errorf("erro ao excluir arquivo"))

	repo := NewS3Repository(s3Api)

	err := repo.Delete("image.png", 1465)

	assert.Error(t, err)
}

func TestS3Repository_DeleteObjectsError(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		DeleteObjects(gomock.Any()).
		Return(nil, fmt.Errorf("erro ao excluir arquivo"))

	repo := NewS3Repository(s3Api)

	err := repo.DeleteObjects([]storageProvider.ArchiveS3Object{{"image.png", 111}, {"image2.png", 222}})

	assert.Error(t, err)
}

func TestS3Repository_DeleteObjects(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		DeleteObjects(gomock.Any()).
		Return(nil, nil)

	repo := NewS3Repository(s3Api)

	err := repo.DeleteObjects([]storageProvider.ArchiveS3Object{{"image.png", 111}, {"image2.png", 222}})

	assert.NoError(t, err)
}

func TestService_uploadFileToS3_public_access_s3(t *testing.T) {

	os.Setenv("S3_BUCKET", "teste")

	controller := gomock.NewController(t)
	defer controller.Finish()
	s3Api := mock.NewMockS3API(controller)

	s3Api.EXPECT().
		PutObject(gomock.Any()).
		Return(nil, nil).
		Times(1)

	s3error := awserr.NewRequestFailure(
		awserr.New(s3control.ErrCodeNoSuchPublicAccessBlockConfiguration, "The public access block configuration was not found", nil),
		404, "")

	s3Api.EXPECT().
		GetPublicAccessBlock(gomock.Any()).
		Return(nil, s3error).
		Times(1)

	repo := NewS3Repository(s3Api)

	err := repo.Upload(nil, 0, "", 0)

	assert.NoError(t, err)

}
