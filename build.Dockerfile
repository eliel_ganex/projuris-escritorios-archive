## Generate OpenApi
FROM openapitools/openapi-generator-cli as openapi

WORKDIR /app

COPY docs ./docs/

RUN docker-entrypoint.sh generate -i docs/ms-archive.json -g go-gin-server -o out
RUN rm -rf out/api out/main.go out/Dockerfile out/.openapi-generator-ignore

## Build App
FROM golang:1.13 as builder

WORKDIR /app

COPY . .
COPY --from=openapi ./app/out /app/out

RUN make build-docker

# Build Image
FROM alpine:3.11.3

RUN apk add libc6-compat tzdata

COPY --from=builder ./app/swagger /swagger
COPY --from=openapi ./app/docs /docs

COPY --from=builder ./app/dist/api-linux /app

ENV GIN_MODE=release

CMD ["/app"]